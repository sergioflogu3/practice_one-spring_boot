package com.retopracticonro1.retopracticonro1.service;
import com.retopracticonro1.retopracticonro1.domain.StringPayload;
import org.springframework.stereotype.Service;

@Service
public class StringUtilitiesService {
    public StringPayload saluteMessages(StringPayload name){
        String temp =  "Hola " + name.string() + ", bienvenido a Spring Boot";
        return new StringPayload(temp);
    }
}
