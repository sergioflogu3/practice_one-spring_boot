package com.retopracticonro1.retopracticonro1.controller;

import com.retopracticonro1.retopracticonro1.domain.StringPayload;
import com.retopracticonro1.retopracticonro1.service.StringUtilitiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UtilitiesController {

    private final StringUtilitiesService stringUtilitiesService;

    @Autowired
    public UtilitiesController(StringUtilitiesService stringUtilitiesService) {
        this.stringUtilitiesService = stringUtilitiesService;
    }

    @PostMapping("/saludo")
    public @ResponseBody StringPayload salute(@RequestBody StringPayload payload) {
        return stringUtilitiesService.saluteMessages(payload);
    }
}
