package com.retopracticonro1.retopracticonro1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RetoPracticoNro1Application {

	public static void main(String[] args) {
		SpringApplication.run(RetoPracticoNro1Application.class, args);
	}

}
