package com.retopracticonro1.retopracticonro1.domain;

public record StringPayload(String string){}
